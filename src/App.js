import React from "react";
import PolicyList from "./components/Policy/PolicyList";
import Page from "./components/Page";

const App = () => (
  <Page>
    <h1 className="page-title">Your policies</h1>
    <PolicyList />
  </Page>
);

export default App;
