import React from "react";
import { shallow, mount } from "enzyme";
import toJSON from "enzyme-to-json";
import PolicyList from "../Policy/PolicyList";
import { act } from "react-dom/test-utils";
import axios from "axios";
import MockAdapter from "axios-mock-adapter";

const response = [
  {
    id: "TRAVEL-COVER-INS",
    type: "travel",
    title: "London to Paris",
    description: "Baggage Cover; Medical Cover; Cancellation Cover",
    status: "active",
    premium: 106.65,
    premium_formatted: "AUD $106.65",
    payment_date: "2019-10-10T13:29:38.814849Z",
    coverage_start_date: "2019-11-17",
    coverage_end_date: "2019-11-19",
    renewal: null,
    partner: {
      id: "labore",
      name: "Labore Inc.",
      logo:
        "https://s3-ap-southeast-2.amazonaws.com/cg-frontend-tests/labore.svg"
    }
  }
];

describe("<PolicyList/>", () => {
  it("Should render a policy list", () => {
    const wrapper = shallow(<PolicyList />);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });

  // it('Should call the API and render a policy list with data', done => {
  //     let component;
  //     const mock = new MockAdapter(axios);
  //     mock.onGet('https://7946a218-d225-4d0e-80ac-450bbc9713a0.mock.pstmn.io/booking').reply(200, response.data);

  //     act(() => {
  //         component = mount(<PolicyList />);
  //       });

  //     expect(mock).toHaveBeenCalled();
  //   });
});
