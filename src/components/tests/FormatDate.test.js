import formatDate from "../../helpers/formatDate";

describe("FormatDate Function", () => {
  it("Removes the timestamp from a given date", () => {
    const randomDate = "2019-10-10T13:29:38.814849Z";
    expect(formatDate(randomDate)).toEqual("Oct 11, 2019");
  });

  it("Formats a date to show the short month format", () => {
    const randomShortDate = "2019-12-17";
    expect(formatDate(randomShortDate)).toEqual("Dec 17, 2019");
  });
});
