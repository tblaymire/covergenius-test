import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import PolicyRenewal from "../Policy/PolicyRenewal";

describe("<PolicyRenewal/>", () => {
  it("Should render a policy renewal with a date and title", () => {
    const fakeRenewal = "22/05/2019";
    const wrapper = shallow(<PolicyRenewal renewal={fakeRenewal} />);
    const date = wrapper.find(".value");
    const title = wrapper.find(".title");
    expect(date.props().children).toBe(fakeRenewal);
    expect(title.props().children).toContain("Renewal");
    expect(toJSON(wrapper)).toMatchSnapshot();
  });
});
