import React from "react";
import { shallow, mount } from "enzyme";
import toJSON from "enzyme-to-json";
import PolicyItem from "../../components/Policy/PolicyItem";

const fakePolicy = {
  id: "TRAVEL-COVER-INS",
  type: "travel",
  title: "London to Paris",
  description: "Baggage Cover; Medical Cover; Cancellation Cover",
  status: "active",
  premium: 106.65,
  premium_formatted: "AUD $106.65",
  payment_date: "2019-10-10T13:29:38.814849Z",
  coverage_start_date: "2019-11-17",
  coverage_end_date: "2019-11-19",
  renewal: null,
  partner: {
    id: "labore",
    name: "Labore Inc.",
    logo: "https://s3-ap-southeast-2.amazonaws.com/cg-frontend-tests/labore.svg"
  }
};

describe("<PolicyItem/>", () => {
  it("Should render a policy item", () => {
    const wrapper = shallow(
      <PolicyItem id={fakePolicy.id} policy={fakePolicy} />
    );
    expect(toJSON(wrapper)).toMatchSnapshot();
  });

  it("Should render the cover title and description", () => {
    const wrapper = shallow(
      <PolicyItem id={fakePolicy.id} policy={fakePolicy} />
    );
    const title = wrapper.find(".policy-title");
    const description = wrapper.find(".policy-description");
    expect(title.props().children).toBe(fakePolicy.title);
    expect(description.props().children).toContain(fakePolicy.description);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });

  it("Should render the toggle button correctly", () => {
    const wrapper = shallow(
      <PolicyItem id={fakePolicy.id} policy={fakePolicy} />
    );
    const button = wrapper.find(".policy-toggle");
    expect(button).toHaveLength(1);
  });

  it("Should render the partner logo correctly", () => {
    const wrapper = shallow(
      <PolicyItem id={fakePolicy.id} policy={fakePolicy} />
    );
    const partnerLogo = wrapper.find(".partner-logo");
    expect(partnerLogo.props().src).toBe(fakePolicy.partner.logo);
    expect(partnerLogo.props().alt).toBe(fakePolicy.partner.name);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });

  it("Should add the active class to the button when clicked", () => {
    const wrapper = shallow(
      <PolicyItem id={fakePolicy.id} policy={fakePolicy} />
    );
    wrapper.find(".policy-toggle").simulate("click");
    const PolicyStyles = wrapper.find("PolicyStyles");
    expect(PolicyStyles.props().active).toBe(true);
  });
});
