import React from "react";
import { shallow } from "enzyme";
import toJSON from "enzyme-to-json";
import PolicyCoverage from "../Policy/PolicyCoverage";

describe("<PolicyCoverage/>", () => {
  it("Should render a policy coverage", () => {
    const wrapper = shallow(<PolicyCoverage />);
    expect(toJSON(wrapper)).toMatchSnapshot();
  });

  it("Should render a policy coverage with a correct start and end date", () => {
    const fakeStartDate = "22 Feb 2019";
    const fakeEndDate = "12 Mar 2020";
    const wrapper = shallow(
      <PolicyCoverage startDate={fakeStartDate} endDate={fakeEndDate} />
    );
    const startDateElement = wrapper.find(".start-date");
    const endDateElement = wrapper.find(".end-date");
    expect(startDateElement.props().children).toContain("Feb 22, 2019");
    expect(endDateElement.props().children).toContain("Mar 12, 2020");
    expect(toJSON(wrapper)).toMatchSnapshot();
  });
});
