import styled from "styled-components";

const PolicyStyles = styled.div`
  width: 100%;
  padding: 16px;
  background: ${props => props.theme.white};
  min-height: 168px;
  margin-bottom: 16px;
  box-shadow: ${props => props.active ? "20px 0px 25px rgba(172, 168, 129, 0.2), 20px 0px 10px rgba(197, 202, 209, 0.56)" : ""};
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  
  @supports (display: grid) {
    display: grid;
    grid-template-rows: auto auto;
    grid-template-columns: 1fr 1fr 0.5fr;
  }

  @media (min-width: 480px) {
    margin-bottom: 32px;
  }

  &:last-child {
    margin-bottom: none;
  }

  .policy-toggle {
    display: none;
    background: none;
    border: none;
    vertical-align: top;
    padding: 0;
    cursor: pointer;
    outline: none;
    img {
      width: 4.2rem;
      height: 4.2rem;
    }
    @media (min-width: 480px) {
      display: inline-block;
    }
  }

  .title {
    font-family: ${props => props.theme.fontSecondary};
    font-size: 1.2rem;
    line-height: 1.8rem;
    color: ${props => props.theme.slate};
  }

  .value {
    text-transform: uppercase;
    font-size: 1.6rem;
    line-height: 2.4rem;
  }

  .policy-title {
    font-size: 2.4rem;
    line-height: 3rem;
    font-weight: ${props => props.theme.fontWeightBold};

    @media (min-width: 768px) and (max-width: 1024px) {
      font-size: 2rem;
    }
  }

  .policy-description {
    color: ${props => props.theme.lightgreyAlt};
    font-weight: normal;
    font-size: 1.4rem;
    line-height: 2.1rem;
    display: block;
  }
`;

PolicyStyles.displayName = 'PolicyStyles';

export default PolicyStyles;
