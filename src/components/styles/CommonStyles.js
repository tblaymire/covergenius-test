import styled from "styled-components";

export const Seperator = styled.div`
  width: 100%;
  height: 1px;
  background: ${props => props.theme.lightgreySecondary};
  margin: 1rem 0;
`;

export const SeperatorVertical = styled.div`
  display: none;
  @media (min-width: 480px) {
    display: block;
    height: 50px;
    width: 1px;
    background: ${props => props.theme.lightgreySecondary};
  }
`;
