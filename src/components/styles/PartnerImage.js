import styled from "styled-components";

const PartnerImage = styled.div`
  align-self: flex-start;

  .partner-logo {
    width: 100%;
    @media (min-width: 480px) {
      padding: 0;
    }
    @media (min-width: 769px) {
      padding-left: 2.3rem;
    }
  }

  @supports (display: grid) {
    align-self: center;
  }

  @media (min-width: 480px) {
    justify-self: end;
    grid-row-start: 1;
    grid-row-end: 1;s
  }

  @media (min-width: 769px) {
    grid-row: 1 / 2;
    grid-column: 3;
    grid-row: 1 / -1;
  }
`;

PartnerImage.displayName = "PartnerImage";

export default PartnerImage;
