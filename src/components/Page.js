import React from "react";
import styled, { ThemeProvider, createGlobalStyle } from "styled-components";

const theme = {
  black: "#2D2D2D",
  lightgreyAlt: "#73777C",
  lightgrey: "#F6F6F6",
  lightgreySecondary: "#E0E4E8",
  slate: "#65737E",
  gold: "#FFD200",
  green: "#00A082",
  red: "#FF3200",
  white: "#FFFFFF",
  maxWidth: "1060px;",
  bs: "0 12px 24px 0 rgba(0, 0, 0, 0.09)",
  fontPrimary: "brandon-grotesque",
  fontSecondary: "bitter",
  headingFont: "3.2rem",
  fontWeightBold: "900"
};

const StyledPage = styled.div`
  background: ${theme.lightgrey};
  color: ${theme.black};
`;

const Inner = styled.div`
  max-width: ${theme.maxWidth};
  margin: 0 auto;
  padding: 1.6rem 1.6rem;
  @media (min-width: 480px) {
    padding: 2.4rem 2.4rem;
  }
  @media (min-width: 768px) {
    padding: 4.8rem 4.8rem;
  }
  .page-title {
    font-size: ${theme.headingFont};
    line-height: 40px;
    text-transform: uppercase;
  }
`;

const GlobalStyle = createGlobalStyle`
  html {
    box-sizing: border-box;
    font-size: 10px;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    padding: 0;
    margin: 0;
    font-size: 1.5rem;
    line-height: 2;
    font-family: ${theme.fontPrimary};
  }
  h1, h2, h3 {
    margin: 0; 
  }
  a {
    text-decoration: none;
    color: ${theme.black};
  }
  button {  font-family: '${theme.fontPrimary}'; }
`;

const Page = props => (
  <ThemeProvider theme={theme}>
    <StyledPage>
      <GlobalStyle />
      <Inner>{props.children}</Inner>
    </StyledPage>
  </ThemeProvider>
);

export default Page;
