import React, { useState, useEffect } from "react";
import styled from "styled-components";
import axios from "axios";
import PolicyItem from "./PolicyItem";
import { ENDPOINT } from "../config";

const PolicyListWrapper = styled.div`
  padding: 24px 0;
`;

function PolicyList() {
  const [data, setData] = useState({ policies: [] });

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(ENDPOINT);
      setData(result.data);
    };
    fetchData();
  }, []);

  return (
    <PolicyListWrapper>
      {data.policies.map(policy => (
        <PolicyItem key={policy.id} policy={policy} />
      ))}
    </PolicyListWrapper>
  );
}

export default PolicyList;
