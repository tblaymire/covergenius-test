import React from "react";
import styled from "styled-components";
import formatDate from "../../helpers/formatDate";

const Coverage = styled.div`
  padding: 0;

  @media (min-width: 480px) {
    padding: 0 1.5rem;
  }

  .coverage-dates {
    display: flex;
    font-size: 1.6rem;
    line-height: 2.4rem;
  }

  .coverage-status {
    font-size: 1.4rem;
    line-height: 2.2rem;
  }

  .active {
    color: ${props => props.theme.green};
  }
  .expired {
    color: ${props => props.theme.red};
  }

  .blob {
    height: 8px;
    width: 8px;
    border-radius: 50%;
    display: inline-block;
    margin-left: 4px;
    margin-bottom: 1px;
    &.blob__active {
      background-color: ${props => props.theme.green};
    }
    &.blob__expired {
      background-color: ${props => props.theme.red};
    }
  }
`;

const CoverageStatus = styled.div`
  text-transform: uppercase;
  font-family: ${props => props.theme.fontPrimary};
  font-weight: ${props => props.theme.fontWeightBold};
  display: flex;
  justify-content: space-between;
  align-items: center;
  .title {
    font-weight: normal;
    text-transform: initial;
  }
`;

const PolicyCoverage = ({ status, startDate, endDate }) => (
  <Coverage>
    <div className="coverage-dates">
      <div className="start-date value">{formatDate(startDate)}</div>
      &nbsp;to&nbsp;
      <div className="end-date value">{formatDate(endDate)}</div>
    </div>

    <CoverageStatus>
      <span className="title">Coverage dates</span>
      {status === "active" ? (
        <div className="coverage-status active">
          {status}
          <span className="blob blob__active" />
        </div>
      ) : (
        <div className="coverage-status expired">
          {status}
          <span className="blob blob__expired" />
        </div>
      )}
    </CoverageStatus>
  </Coverage>
);

export default PolicyCoverage;
