import React, { useState } from "react";
import styled from "styled-components";
import formatDate from "../../helpers/formatDate";
import PolicyCoverage from "./PolicyCoverage";
import PolicyRenewal from "./PolicyRenewal";
import PolicyStyles from "../styles/PolicyStyles";
import { Seperator, SeperatorVertical } from "../styles/CommonStyles";
import PartnerImage from "../styles/PartnerImage";
import chevronRightIcon from "../../assets/icons/icon-32-circle-chevronRight.svg";
import chevronDownIcon from "../../assets/icons/icon-32-circle-chevronDown.svg";

const PolicyOverview = styled.div`
  white-space: initial;
  float: right;

  @supports (display: grid) {
    grid-column: 1 / -1;
    align-self: center;
  }

  @media (min-width: 480px) {
    grid-row: 1 / 2;
    grid-column: 1 / 3;
    white-space: nowrap;
  }

  @media (min-width: 765px) {
    white-space: nowrap;
  }

  .policy-content {
    display: inline-block;
    padding: 0;

    @media (min-width: 480px) {
      padding-left: 1.6rem;
    }
  }
`;

const PolicyData = styled.div`
  display: flex;

  @supports (display: grid) {
    grid-row: 2 / 3;
    grid-column: 1 / 3;
    align-self: center;
  }
`;

const PolicyPremium = styled.div`
  display: none;

  @media (min-width: 480px) {
    flex-direction: column;
    padding: 0 1.5rem;
    display: flex;
  }
`;

const PolicyPayment = styled.div`
  display: none;

  @media (min-width: 480px) {
    flex-direction: column;
    padding-right: 1.5rem;
    display: flex;
  }
`;

const PolicyItem = ({ policy }) => {
  const [active, toggleActive] = useState(false);

  return (
    <PolicyStyles active={active}>
      <PolicyOverview>
        <button className="policy-toggle" onClick={() => toggleActive(!active)}>
          {active ? (
            <img src={chevronDownIcon} alt="Select policy button" />
          ) : (
            <img src={chevronRightIcon} alt="De-select policy button" />
          )}
        </button>
        <div className="policy-content">
          <span className="policy-title">{policy.title}</span>
          <span className="policy-description">
            {policy.id} | {policy.description}
          </span>
        </div>
        <Seperator />
      </PolicyOverview>

      <PolicyData>
        <PolicyPayment>
          <span className="value">{formatDate(policy.payment_date)}</span>
          <span className="title">Payment date</span>
        </PolicyPayment>

        <SeperatorVertical />

        <PolicyCoverage
          status={policy.status}
          startDate={policy.coverage_start_date}
          endDate={policy.coverage_end_date}
        />

        <SeperatorVertical />

        <PolicyPremium>
          <span className="value">AU ${policy.premium}</span>
          <span className="title">Price/Premium</span>
        </PolicyPremium>

        {policy.renewal !== null && (
          <React.Fragment>
            <SeperatorVertical />
            <PolicyRenewal renewal={policy.renewal} />
          </React.Fragment>
        )}
      </PolicyData>

      <PartnerImage>
        <img
          className="partner-logo"
          src={policy.partner.logo}
          alt={policy.partner.name}
        />
      </PartnerImage>
    </PolicyStyles>
  );
};

export default PolicyItem;
