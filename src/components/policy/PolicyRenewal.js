import React from "react";
import styled from "styled-components";

const Renewal = styled.div`
  display: none;

  @media (min-width: 480px) {
    flex-direction: column;
    padding: 0 1.5rem;
    display: flex;
  }
`;

const PolicyRenewal = ({ renewal }) => (
  <React.Fragment>
    <Renewal>
      <span className="value">{renewal}</span>
      <span className="title">Renewal</span>
    </Renewal>
  </React.Fragment>
);

export default PolicyRenewal;
