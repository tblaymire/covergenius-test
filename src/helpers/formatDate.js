export default function(date) {

    let formattedDate = new Date(date);

    const options = {
        year: 'numeric',
        month: 'short',
        day: 'numeric' 
    };
    
    return formattedDate.toLocaleDateString('en-GB', options)
}
  